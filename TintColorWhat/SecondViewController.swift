
import UIKit

class SecondViewController: UIViewController {

    override func viewDidLoad() {
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Ignore This One", style: .Plain, target: self, action: "doNothing")
    }
    
    func doNothing() {
        print("Did nothing on purpose")
    }
    
    @IBAction func launchWasTapped(sender: AnyObject) {

        
        // from http://nshipster.com/uialertcontroller/
        let alertController = UIAlertController(title: "Default Style", message: "A standard alert.", preferredStyle: .Alert)
        
        let cancelAction = UIAlertAction(title: "OK", style: .Cancel) { (action) in
            // ...
        }
        alertController.addAction(cancelAction)
        
        self.presentViewController(alertController, animated: true) {
        }
    }
}
