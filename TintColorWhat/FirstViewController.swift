
import UIKit

class FirstViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Home", style: .Plain, target: self, action: "homeWasTapped")
        
    }

    func homeWasTapped() {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
}

